<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
<style>
img{
width:150px;
height:150px;
}

.produit{
display:flex;
flex-flow : row wrap;
justify-content: space-around;
}
</style>
</head>
<body>
<h1>PAGE VENTE HIGH TECH</h1>
	<div class="produit">
		<c:forEach items="${ articles }" var="article">
			<div class="c">
				<h4>
					<c:out value="${ article.nom }"></c:out>
				</h4>
				<p>
					<c:out value="${ article.description }"></c:out>
				</p>
				<p>
				<img class="art" src="${ article.image }"
					alt="${ article.nom }"> 
				</p>
				<p>	
				<c:out value="${ article.prix } euro">
				</c:out>
				</p> 
			</div>
		</c:forEach>
	</div>
	<button name="bouton1" onclick="http:/localhost:8080/Suzy_exercice/inscription">vous inscrire</button>
</body>
</html>
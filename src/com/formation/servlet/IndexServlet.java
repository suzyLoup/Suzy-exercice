package com.formation.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.formation.model.Article;

/**
 * Servlet implementation class IndexServlet
 */
@WebServlet("/IndexServlet")
public class IndexServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public IndexServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Article> articles = new ArrayList<Article>();
		articles.add(new Article("t�l�", "�cran plat", 589.99,
				"https://image.darty.com/hifi_video/tous_ecrans_plats/qled/samsung_qe75q8c_4k_uhd_o1703064299329B_152028785.jpg"));
		articles.add(new Article("Music", "chaine hifi", 289.50,
				"http://cdn01.acidaudio.fr/media/catalog/product/cache/1/image/9df78eab33525d08d6e5fb8d27136e95/1/4/14872.jpg"));
		articles.add(new Article("PS4", "console jeux vid�o", 399.99,
				"https://i2.cdscdn.com/pdt2/3/4/2/1/700x700/0711719863342/rw/ps4-1-to-star-wars-battlefront.jpg"));
		articles.add(new Article("Hololens", "casque de r�alit� virtuelle", 599.29,
				"https://www.sciencesetavenir.fr/assets/img/2015/01/22/cover-r4x3w1000-57df66d6625d4-hololens.jpg"));
		
		request.setAttribute("articles", articles);
		
		
		this.getServletContext().getRequestDispatcher("/WEB-INF/views/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

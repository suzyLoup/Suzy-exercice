package com.formation.model;

import java.util.Date;

public class Utilisateur {
	
	private String nom_user;
	private String prenom_user;
	private String mdp;
	private String anniv;
	
	public Utilisateur() {
		
	}
	
	public Utilisateur(String nom_user, String prenom_user, String mdp, String anniv) {
		this.nom_user=nom_user;
		this.prenom_user=prenom_user;
		this.mdp=mdp;
		this.anniv=anniv;
	}
	
	public String getNom_user() {
		return nom_user;
	}
	public void setNom_user(String nom_user) {
		this.nom_user = nom_user;
	}
	public String getPrenom_user() {
		return prenom_user;
	}
	public void setPrenom_user(String prenom_user) {
		this.prenom_user = prenom_user;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public String getAnniv() {
		return anniv;
	}

	public void setAnniv(String anniv) {
		this.anniv = anniv;
	}
	

}
